import * as React from 'react';
import { useEffect, useState } from 'react';
import { Squash as Hamburger } from 'hamburger-react';
import TreeView from '@mui/lab/TreeView';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import TreeItem from '@mui/lab/TreeItem';
import VideoList from './components/videoList/videoList';
import './App.css';


function App() {

  const videos = [
    {
      title: 'Video 1',
      src: '/1.mp4',
      duration: 22,
    },
    {
      title: 'Video 2',
      src: '/2.mp4',
      duration: 5,
    },
    {
      title: 'Video 3',
      src: '/3.mp4',
      duration: 5,
    },
    {
      title: 'Video 3',
      src: '/4.mp4',
      duration: 5,
    },
    {
      title: 'Video 3',
      src: '/5.mp4',
      duration: 5,
    },
    {
      title: 'Video 3',
      src: '/6.mp4',
      duration: 5,
    },  {
      title: 'Video 3',
      src: '/7.mp4',
      duration: 5,
    },
    {
      title: 'Video 3',
      src: '/8.mp4',
      duration: 5,
    },
    {
      title: 'Video 3',
      src: '/9.mp4',
      duration: 5,
    },
  ];

  const treeData = [
    {
      key: "0",
      label: "Documents",
      icon: "fa fa-folder",
      title: "Documents Folder",
      children: [
        {
          key: "0-0",
          label: "Document 1-1",
          icon: "fa fa-folder",
          title: "Documents Folder",
          children: [
            {
              key: "0-1-1",
              label: "Document-0-1.doc",
              icon: "fa fa-file",
              title: "Documents Folder",
            },
            {
              key: "0-1-2",
              label: "Document-0-2.doc",
              icon: "fa fa-file",
              title: "Documents Folder",
            },
            {
              key: "0-1-3",
              label: "Document-0-3.doc",
              icon: "fa fa-file",
              title: "Documents Folder",
            },
            {
              key: "0-1-4",
              label: "Document-0-4.doc",
              icon: "fa fa-file",
              title: "Documents Folder",
            },
          ],
        },
      ],
    },
    {
      key: "1",
      label: "Desktop",
      icon: "fa fa-desktop",
      title: "Desktop Folder",
      children: [
        {
          key: "1-0",
          label: "document1.doc",
          icon: "fa fa-file",
          title: "Documents Folder",
        },
        {
          key: "0-0",
          label: "documennt-2.doc",
          icon: "fa fa-file",
          title: "Documents Folder",
        },
      ],
    },
    {
      key: "2",
      label: "Downloads",
      icon: "fa fa-download",
      title: "Downloads Folder",
      children: [],
    },
  ];

  const fileTreeData = [{
              "operation_id":"0_9_1",
              "job_type":"Front Tire Change",
              "car_details":{
                  "Make":"Chevrolet",
                  "Model":"Volt",
                  "Year":"2016"
              },
              "timestarted":"1679007685383"
          }]
    

  //View Tree functionality
  const [blackOverlay, setOverlay] = useState()
  const openCloseOverlay = () => {
    if(!blackOverlay){
      document.getElementById('black').style.display = "block"
      setOverlay(!blackOverlay)
    }
    if(blackOverlay){
      document.getElementById('black').style.display = "none"
      setOverlay(!blackOverlay)
    }
  }

  return (
    <div className="App">
      <div className='nav'>
        <a href='/'><img src="/rt-logo-512.png" className='logo'/></a>
        <span title="View Job Tree" onClick={openCloseOverlay}><Hamburger color="#fff" /></span>
      </div>

      <div className='blackOut' id='black'>
        <div className='testing'>
            <TreeView
              aria-label="multi-select"
              defaultCollapseIcon={<ExpandMoreIcon />}
              defaultExpandIcon={<ChevronRightIcon />}
              multiSelect
              sx={{ height: 216, flexGrow: 1, maxWidth: 400, overflowY: 'auto' }}
            >
              <TreeItem nodeId="1" label="Job ID - 0123">
                <TreeItem nodeId="2" label="Steps" />
                  <TreeItem nodeId="2" label="step1"></TreeItem>
                <TreeItem nodeId="3" label="Chrome" />
                
                <TreeItem nodeId="4" label="Webstorm" />
              </TreeItem>
              <TreeItem nodeId="5" label="Documents">
                <TreeItem nodeId="6" label="MUI">
                  <TreeItem nodeId="7" label="src">
                    <TreeItem nodeId="8" label="index.js" />
                    <TreeItem nodeId="9" label="tree-view.js" />
                  </TreeItem>
                </TreeItem>
              </TreeItem>
            </TreeView>
        </div>
      </div>
      
      <div className='videoStreamContainer'>
        
        <div className='card'>
          <div id="data">
            <div className='viewData'>
              <VideoList videos={videos} />
            </div>
          </div>
        </div>

        <div className='card'>Passenger Front</div>
        <div className='card'>Driver Rear</div>
        <div className='card'>Passenger Rear</div>
        <div className='card'>Driver Clocking</div>
        <div className='card'>Passenger Clocking</div>

      </div>

      <div className='jobInformation'>Job Information
      </div>
    </div>
  )
}

export default App
