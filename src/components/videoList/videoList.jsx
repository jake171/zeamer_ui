import React, { useState, useEffect } from 'react';
import "./videoList.css"
const VideoList = ({ videos }) => {
  const [currentVideoIndex, setCurrentVideoIndex] = useState(0);
  const [currentTime, setCurrentTime] = useState(0);

  const [pause, setPause] = useState(false)
  const [seek_interval, setSeekInterval] = useState(0)

  useEffect(() => {
    const interval = setInterval(() => {
      if (!pause){
        setCurrentTime(prevTime => prevTime + .1);

      }
    }, 100);

    return () => {clearInterval(interval);}
  }, [pause]);


  useEffect(() => {
     if (!pause && currentTime >= videos[currentVideoIndex].duration) {
      setCurrentVideoIndex(prevIndex =>
        prevIndex < videos.length - 1 ? prevIndex + 1 : 0
      );
      console.log(currentVideoIndex);
      setCurrentTime(0);
      
    }
  }, [currentTime, currentVideoIndex, videos]);


  useEffect(()=>{

    if (seek_interval<0){
      console.log("seek changed", seek_interval,currentVideoIndex,document.getElementById("video1").currentTime );
      if(document.getElementById("video1").currentTime>Math.abs(seek_interval)){
        document.getElementById("video1").currentTime=document.getElementById("video1").currentTime+seek_interval;
        setSeekInterval(0);
        setCurrentTime(prevTime=>prevTime+seek_interval);
      }else if (document.getElementById("video1").currentTime<=Math.abs(seek_interval) && currentVideoIndex==0){
        document.getElementById("video1").currentTime=0;
        setSeekInterval(0);
        setCurrentTime(0);
      }else if (document.getElementById("video1").currentTime<=Math.abs(seek_interval) && currentVideoIndex>=1){
        console.log("We are here");
        var video_current_time=document.getElementById("video1").currentTime
        setCurrentVideoIndex(prevIndex=> prevIndex > 0 ? prevIndex - 1 : 0);
        currentVideo = videos[currentVideoIndex];
        document.getElementById("video1").currentTime=seek_interval-video_current_time;
        let subtractThis = seek_interval + video_current_time
        setSeekInterval(subtractThis);
      }
    }

  }, [seek_interval]);

  var currentVideo = videos[currentVideoIndex];

  //Slider functionality
  const [value, setValue] = useState(0);
  const MAX = 100;
  const getBackgroundSize = () => {
    return {
      backgroundSize: `${(value * 100) / MAX}% 100%`,
    };
  };

  //Video Controls
  const driverFrontVid = document.getElementById("video1"); 
  // console.log(driverFrontVid)
  const playPauseButton = () => {
    if(pause){
      document.getElementById('play').src = "/pause.svg"
      driverFrontVid.play(); 
      setPause(!pause)
    }
    if(!pause){
      document.getElementById('play').src = "/play.svg"
      driverFrontVid.pause();
      setPause(!pause)
    }
  }
  const seekN10Button = () => {
    console.log("SeekN10");
    setSeekInterval(-10);
  }
  const seekP10Button = () => {
    console.log("SeekP10");
    setSeekInterval(10);
  }
  return (
    <div id={this}>
      <video id="video1" src={currentVideo.src} preload={"true"} muted autoPlay width={"150%"}/>
      <div className='scrub'>
      <input
        id='videoSlide'
        type="range"
        min="0"
        max={MAX}
        onChange={(e) => setValue(e.target.value)}
        style={getBackgroundSize()}
        value={value}
      />
        <div className='controlsContainer'>
          <img src='/rewind.svg' onClick={seekN10Button} width={"10%"} id="rewind"></img>
          <img src='/pause.svg' onClick={playPauseButton} width={"20%"} id="play"></img>
          <img src='/forward.png' onClick={seekP10Button} width={"10%"} id="forward"></img>
        </div>
        <img src='/profile.svg' width={"2.5%"} id="profile"></img>
      </div>
    </div>
  );
};

export default VideoList;